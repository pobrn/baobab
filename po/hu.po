# Hungarian translation for baobab.
# Copyright (C) 1999, 2000, 2001, 2002, 2003, 2004, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2016, 2018, 2019, 2020, 2022, 2023. Free Software Foundation, Inc.
# This file is distributed under the same license as the baobab package.
#
# Szabolcs Ban <shooby at gnome dot hu>, 1999, 2000.
# Gergely Nagy <greg at gnome dot hu>, 2001.
# Andras Timar <timar at gnome dot hu>, 2001, 2002, 2003.
# Gabor Sari <saga at gnome dot hu>, 2003, 2004.
# Laszlo Dvornik <dvornik at gnome dot hu>, 2004.
# Gabor Kelemen <kelemeng at gnome dot hu>, 2004, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013.
# Mate ORY <orymate at gmail d0t com>, 2006.
# Lukács Bence <lukacs.bence1 at gmail dot com>, 2012.
# Balázs Úr <ur.balazs at fsf dot hu>, 2012, 2013, 2014, 2016, 2018, 2019, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: baobab master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/baobab/issues\n"
"POT-Creation-Date: 2023-07-04 22:01+0000\n"
"PO-Revision-Date: 2023-07-25 23:37+0200\n"
"Last-Translator: Balázs Úr <ur.balazs at fsf dot hu>\n"
"Language-Team: Hungarian <openscope at fsf dot hu>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#: data/org.gnome.baobab.appdata.xml.in:6 data/org.gnome.baobab.desktop.in:3
#: data/ui/baobab-main-window.ui:35 src/baobab-window.vala:267
msgid "Disk Usage Analyzer"
msgstr "Lemezhasználat-elemző"

#: data/org.gnome.baobab.appdata.xml.in:7 data/org.gnome.baobab.desktop.in:4
msgid "Check folder sizes and available disk space"
msgstr "Mappaméretek és elérhető lemezterület vizsgálata"

#: data/org.gnome.baobab.appdata.xml.in:9
msgid ""
"A simple application to keep your disk usage and available space under "
"control."
msgstr ""
"Egy egyszerű alkalmazás a lemezhasználat és az elérhető hely ellenőrzés "
"alatt tartásához."

#: data/org.gnome.baobab.appdata.xml.in:12
msgid ""
"Disk Usage Analyzer can scan specific folders, storage devices and online "
"accounts. It provides both a tree and a graphical representation showing the "
"size of each folder, making it easy to identify where disk space is wasted."
msgstr ""
"A lemezhasználat-elemző képes megvizsgálni bizonyos mappákat, "
"tárolóeszközöket és online fiókokat. Mind fa, mind grafikus ábrázolást "
"szolgáltat az egyes mappák méretének megjelenítéséhez, megkönnyítve annak "
"azonosítását, hogy hol vész el a lemezterület."

#: data/org.gnome.baobab.appdata.xml.in:20
msgid "Devices and Locations View"
msgstr "Eszközök és helyek nézet"

#: data/org.gnome.baobab.appdata.xml.in:24
msgid "Scan View"
msgstr "Vizsgálat nézet"

#: data/org.gnome.baobab.appdata.xml.in:39 src/baobab-window.vala:269
msgid "The GNOME Project"
msgstr "A GNOME projekt"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.baobab.desktop.in:6
msgid "storage;space;cleanup;"
msgstr "tároló;lemezterület;takarítás;"

#: data/org.gnome.baobab.gschema.xml:9
msgid "Excluded locations URIs"
msgstr "Kihagyott helyek URI-címei"

#: data/org.gnome.baobab.gschema.xml:10
msgid "A list of URIs for locations to be excluded from scanning."
msgstr "A vizsgálatból kihagyandó helyek URI-címeinek listája."

#: data/org.gnome.baobab.gschema.xml:20
msgid "Active Chart"
msgstr "Aktív diagram"

#: data/org.gnome.baobab.gschema.xml:21
msgid "Which type of chart should be displayed."
msgstr "Melyik diagramtípus legyen látható."

#: data/org.gnome.baobab.gschema.xml:25
msgid "Window size"
msgstr "Ablakméret"

#: data/org.gnome.baobab.gschema.xml:26
msgid "The initial size of the window"
msgstr "Az ablak kezdeti mérete"

#: data/org.gnome.baobab.gschema.xml:30
msgid "Window Maximized"
msgstr "Teljes méretű ablak"

#: data/org.gnome.baobab.gschema.xml:31
msgid "Whether or not the window is maximized"
msgstr "Az ablak teljes méretű-e"

#: data/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Általános"

#: data/gtk/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show help"
msgstr "Súgó megjelenítése"

#: data/gtk/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Show / Hide primary menu"
msgstr "Elsődleges menü megjelenítése vagy elrejtése"

#: data/gtk/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Quit"
msgstr "Kilépés"

#: data/gtk/help-overlay.ui:33
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "Gyorsbillentyűk megjelenítése"

#: data/gtk/help-overlay.ui:39
msgctxt "shortcut window"
msgid "Show preferences"
msgstr "Beállítások megjelenítése"

#: data/gtk/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Scanning"
msgstr "Vizsgálat"

#: data/gtk/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Scan folder"
msgstr "Mappa vizsgálata"

#: data/gtk/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Rescan current location"
msgstr "Jelenlegi hely ismételt vizsgálata"

#: data/gtk/menus.ui:7 data/ui/baobab-treeview-menu.ui:6
msgid "_Open Externally"
msgstr "_Megnyitás külsőleg"

#: data/gtk/menus.ui:11 data/ui/baobab-treeview-menu.ui:10
msgid "_Copy Path to Clipboard"
msgstr "Út_vonal másolása a vágólapra"

#: data/gtk/menus.ui:15 data/ui/baobab-treeview-menu.ui:14
msgid "Mo_ve to Trash"
msgstr "Át_helyezés a Kukába"

#: data/gtk/menus.ui:21
msgid "Go to _Parent Folder"
msgstr "Ugrás a _szülőmappába"

#: data/gtk/menus.ui:27
msgid "Zoom _In"
msgstr "_Nagyítás"

#: data/gtk/menus.ui:31
msgid "Zoom _Out"
msgstr "_Kicsinyítés"

#: data/ui/baobab-folder-display.ui:14 data/ui/baobab-main-window.ui:142
msgid "Folder"
msgstr "Mappa"

#: data/ui/baobab-folder-display.ui:37 data/ui/baobab-main-window.ui:169
msgid "Size"
msgstr "Méret"

#: data/ui/baobab-folder-display.ui:53 data/ui/baobab-main-window.ui:185
msgid "Contents"
msgstr "Tartalom"

#: data/ui/baobab-folder-display.ui:69 data/ui/baobab-main-window.ui:201
msgid "Modified"
msgstr "Módosítva"

#: data/ui/baobab-location-list.ui:18
msgid "This Device"
msgstr "Ez az eszköz"

#: data/ui/baobab-location-list.ui:42
msgid "Remote Locations"
msgstr "Távoli helyek"

#: data/ui/baobab-main-window.ui:7
msgid "Scan Folder…"
msgstr "Mappa vizsgálata…"

#: data/ui/baobab-main-window.ui:11
msgid "Clear Recent List"
msgstr "Legutóbbiak listájának törlése"

#: data/ui/baobab-main-window.ui:17 data/ui/baobab-preferences-dialog.ui:6
msgid "Preferences"
msgstr "Beállítások"

#: data/ui/baobab-main-window.ui:21
msgid "Keyboard _Shortcuts"
msgstr "_Gyorsbillentyűk"

#: data/ui/baobab-main-window.ui:25
msgid "_Help"
msgstr "_Súgó"

#: data/ui/baobab-main-window.ui:29
msgid "_About Disk Usage Analyzer"
msgstr "A Lemezhasználat-elemző _névjegye"

#: data/ui/baobab-main-window.ui:45 src/baobab-window.vala:540
msgid "Devices & Locations"
msgstr "Eszközök és helyek"

#: data/ui/baobab-main-window.ui:55
msgid "Main Menu"
msgstr "Főmenü"

#: data/ui/baobab-main-window.ui:97
msgid "Rescan Current Location"
msgstr "Jelenlegi hely ismételt vizsgálata"

#: data/ui/baobab-main-window.ui:107
msgid "Files may take more space than shown"
msgstr "A fájlok több helyet foglalhatnak a feltüntetettnél"

#: data/ui/baobab-main-window.ui:272
msgid "Rings Chart"
msgstr "Gyűrűdiagram"

#: data/ui/baobab-main-window.ui:285
msgid "Treemap Chart"
msgstr "Fadiagram"

#: data/ui/baobab-preferences-dialog.ui:12
msgid "Locations to Ignore"
msgstr "Figyelmen kívül hagyandó helyek"

#: src/baobab-application.vala:33
msgid ""
"Do not skip directories on different file systems. Ignored if DIRECTORY is "
"not specified."
msgstr ""
"Ne hagyja ki a különböző fájlrendszeren található könyvtárakat. Figyelmen "
"kívül lesz hagyva, ha nincs megadva KÖNYVTÁR."

#: src/baobab-application.vala:34
msgid "Print version information and exit"
msgstr "Verzióinformációk kiírása és kilépés"

#: src/baobab-cellrenderers.vala:34
#, c-format
msgid "%d item"
msgid_plural "%d items"
msgstr[0] "%d elem"
msgstr[1] "%d elem"

#. Translators: when the last modified time is unknown
#: src/baobab-cellrenderers.vala:40 src/baobab-location-list.vala:80
msgid "Unknown"
msgstr "Ismeretlen"

#. Translators: when the last modified time is today
#: src/baobab-cellrenderers.vala:48
msgid "Today"
msgstr "Ma"

#. Translators: when the last modified time is "days" days ago
#: src/baobab-cellrenderers.vala:53
#, c-format
msgid "%lu day"
msgid_plural "%lu days"
msgstr[0] "%lu nap"
msgstr[1] "%lu nap"

#. Translators: when the last modified time is "months" months ago
#: src/baobab-cellrenderers.vala:58
#, c-format
msgid "%lu month"
msgid_plural "%lu months"
msgstr[0] "%lu hónap"
msgstr[1] "%lu hónap"

#. Translators: when the last modified time is "years" years ago
#: src/baobab-cellrenderers.vala:62
#, c-format
msgid "%lu year"
msgid_plural "%lu years"
msgstr[0] "%lu év"
msgstr[1] "%lu év"

#: src/baobab-location-list.vala:67
#, c-format
msgid "%s Total"
msgstr "%s összesen"

#: src/baobab-location-list.vala:71
#, c-format
msgid "%s Available"
msgstr "%s elérhető"

#. useful for some remote mounts where we don't know the
#. size but do have a usage figure
#: src/baobab-location-list.vala:85
#, c-format
msgid "%s Used"
msgstr "%s használt"

#: src/baobab-location-list.vala:87
msgid "Unmounted"
msgstr "Leválasztva"

#: src/baobab-location.vala:72
msgid "Home Folder"
msgstr "Saját mappa"

#: src/baobab-location.vala:107
msgid "Computer"
msgstr "Számítógép"

#. The only activatable row is "Add location"
#: src/baobab-preferences-dialog.vala:53
msgid "Select Location to Ignore"
msgstr "Válasszon egy figyelmen kívül hagyandó helyet"

#: src/baobab-preferences-dialog.vala:90
msgid "Add Location…"
msgstr "Hely hozzáadása…"

#: src/baobab-window.vala:183
msgid "Select Folder"
msgstr "Mappa kiválasztása"

#: src/baobab-window.vala:190
msgid "Recursively analyze mount points"
msgstr "Csatolási pontok rekurzív elemzése"

#: src/baobab-window.vala:214
#| msgid "Could not analyze volume."
msgid "Could not analyze volume"
msgstr "Nem sikerült elemezni a kötetet"

#: src/baobab-window.vala:276
msgid "translator-credits"
msgstr ""
"Bán Szabolcs <shooby at gnome dot hu>\n"
"Dvornik László <dvornik at gnome dot hu>\n"
"Kelemen Gábor <kelemeng at gnome dot hu>\n"
"Lukács Bence <lukacs.bence1 at gmail dot com>\n"
"Meskó Balázs <mesko dot balazs at fsf dot hu>\n"
"Nagy Gergely <greg at gnome dot hu>\n"
"Őry Máté <orymate at gmail dot com>\n"
"Sári Gábor <saga at gnome dot hu>\n"
"Tímár András <timar at gnome dot hu>"

#: src/baobab-window.vala:341
msgid "Failed to open file"
msgstr "Nem sikerült megnyitni a fájlt"

#: src/baobab-window.vala:358
#| msgid "Failed to open file"
msgid "Failed to trash file"
msgstr "Nem sikerült a kukába dobni a fájlt"

#: src/baobab-window.vala:592
#| msgid "Could not scan folder “%s”"
msgid "Could not scan folder"
msgstr "Nem sikerült megvizsgálni a mappát"

#: src/baobab-window.vala:608
msgid "Scan completed"
msgstr "Vizsgálat befejezve"

#: src/baobab-window.vala:609
#, c-format
msgid "Completed scan of “%s”"
msgstr "A(z) „%s” vizsgálata befejeződött"

#: src/baobab-window.vala:649
#, c-format
msgid "“%s” is not a valid folder"
msgstr "„%s” nem érvényes mappa"
