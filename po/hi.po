# Hindi translation of baobab.
# Copyright (C) 1998-2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the baobab package.
# Panwar108 <caspian7pena@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: baobab master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/baobab/issues/\n"
"POT-Creation-Date: 2024-03-21 17:59+0000\n"
"PO-Revision-Date: 2020-09-19 16:37+0530\n"
"Last-Translator: Panwar108 <caspian7pena@gmail.com>\n"
"Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.1\n"
"X-Language: hi_IN\n"
"X-Source-Language: en\n"

#: data/org.gnome.baobab.appdata.xml.in:6 data/org.gnome.baobab.desktop.in:3
#: data/ui/baobab-main-window.ui:35 src/baobab-window.vala:267
msgid "Disk Usage Analyzer"
msgstr "डिस्क उपयोग विश्लेषक"

#: data/org.gnome.baobab.appdata.xml.in:7 data/org.gnome.baobab.desktop.in:4
msgid "Check folder sizes and available disk space"
msgstr "फोल्डर आकर व उपलब्ध डिस्क स्पेस जाँच"

#: data/org.gnome.baobab.appdata.xml.in:9
msgid ""
"A simple application to keep your disk usage and available space under "
"control."
msgstr "डिस्क उपयोग व उपलब्ध स्पेस नियंत्रित रखने हेतु सरल अनुप्रयोग।"

#: data/org.gnome.baobab.appdata.xml.in:12
msgid ""
"Disk Usage Analyzer can scan specific folders, storage devices and online "
"accounts. It provides both a tree and a graphical representation showing the "
"size of each folder, making it easy to identify where disk space is wasted."
msgstr ""
"डिस्क उपयोग विश्लेषक निर्दिष्ट फोल्डर, संचय उपकरण व ऑनलाइन अकाउंट स्कैन कर सकता है। "
"डिस्क उपयोग पहचान सरलीकृत करने हेतु यह प्रत्येक फोल्डर का आकार शाखानुमा ढांचे व चित्रात्मक "
"रूप से प्रदर्शित करता है।"

#: data/org.gnome.baobab.appdata.xml.in:20
#| msgid "Devices and Locations"
msgid "Devices and Locations View"
msgstr "उपकरण व स्थान दृश्य"

#: data/org.gnome.baobab.appdata.xml.in:24
msgid "Scan View"
msgstr "स्केन दृश्य"

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.baobab.appdata.xml.in:43 src/baobab-window.vala:269
msgid "The GNOME Project"
msgstr "गनोम परियोजना"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.baobab.desktop.in:6
msgid "storage;space;cleanup;"
msgstr "संचय;स्पेस;अनुकूलन;"

#: data/org.gnome.baobab.gschema.xml:9
msgid "Excluded locations URIs"
msgstr "अपवर्जित यूआरआई स्थान"

#: data/org.gnome.baobab.gschema.xml:10
msgid "A list of URIs for locations to be excluded from scanning."
msgstr "स्कैन हेतु अपवर्जित यूआरआई स्थानों की सूची।"

#: data/org.gnome.baobab.gschema.xml:20
msgid "Active Chart"
msgstr "प्रयुक्त चित्र"

#: data/org.gnome.baobab.gschema.xml:21
msgid "Which type of chart should be displayed."
msgstr "प्रदर्शन हेतु चित्र प्रकार निर्दिष्ट करें।"

#: data/org.gnome.baobab.gschema.xml:25
msgid "Window size"
msgstr "विंडो आकार"

#: data/org.gnome.baobab.gschema.xml:26
msgid "The initial size of the window"
msgstr "विंडो का आरंभिक आकार"

#: data/org.gnome.baobab.gschema.xml:30
#| msgid "Window size"
msgid "Window Maximized"
msgstr "विंडो आकार अधिकतम"

#: data/org.gnome.baobab.gschema.xml:31
msgid "Whether or not the window is maximized"
msgstr ""

#: data/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "सामान्य"

#: data/gtk/help-overlay.ui:15
msgctxt "shortcut window"
msgid "Show help"
msgstr "सहायता देखें"

#: data/gtk/help-overlay.ui:21
msgctxt "shortcut window"
msgid "Show / Hide primary menu"
msgstr "प्राथमिक मेन्यू दिखाएँ/छुपाएँ"

#: data/gtk/help-overlay.ui:27
msgctxt "shortcut window"
msgid "Quit"
msgstr "बंद करें"

#: data/gtk/help-overlay.ui:33
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "कुंजीपटल शॉर्टकट देखें"

#: data/gtk/help-overlay.ui:39
#| msgid "Preferences"
msgctxt "shortcut window"
msgid "Show preferences"
msgstr "वरीयताएँ दिखाएँ"

#: data/gtk/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Scanning"
msgstr "स्कैन हो रहा है"

#: data/gtk/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Scan folder"
msgstr "फोल्डर स्कैन करें"

#: data/gtk/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Rescan current location"
msgstr "वर्तमान स्थान पुनः स्कैन करें"

#: data/gtk/menus.ui:7 data/ui/baobab-treeview-menu.ui:6
msgid "_Open Externally"
msgstr ""

#: data/gtk/menus.ui:11 data/ui/baobab-treeview-menu.ui:10
msgid "_Copy Path to Clipboard"
msgstr "पथ क्लिपबोर्ड पर कॉपी करें (_C)"

#: data/gtk/menus.ui:15 data/ui/baobab-treeview-menu.ui:14
msgid "Mo_ve to Trash"
msgstr "ट्रैश में भेजें (_v)"

#: data/gtk/menus.ui:21
#| msgid "Go to _parent folder"
msgid "Go to _Parent Folder"
msgstr "मूल फोल्डर पर जाएँ (_p)"

#: data/gtk/menus.ui:27
#| msgid "Zoom _in"
msgid "Zoom _In"
msgstr "आकार बढ़ाएँ (_I)"

#: data/gtk/menus.ui:31
#| msgid "Zoom _out"
msgid "Zoom _Out"
msgstr "आकार घटाएँ (_O)"

#: data/ui/baobab-folder-display.ui:14 data/ui/baobab-main-window.ui:141
msgid "Folder"
msgstr "फोल्डर"

#: data/ui/baobab-folder-display.ui:37 data/ui/baobab-main-window.ui:168
msgid "Size"
msgstr "आकार"

#: data/ui/baobab-folder-display.ui:53 data/ui/baobab-main-window.ui:184
msgid "Contents"
msgstr "सामग्री"

#: data/ui/baobab-folder-display.ui:69 data/ui/baobab-main-window.ui:200
msgid "Modified"
msgstr "परिवर्तित"

#: data/ui/baobab-location-list.ui:18
msgid "This Device"
msgstr ""

#: data/ui/baobab-location-list.ui:42
msgid "Remote Locations"
msgstr "दूरस्थ स्थान"

#: data/ui/baobab-main-window.ui:7
msgid "Scan Folder…"
msgstr "फोल्डर स्कैन करें..."

#: data/ui/baobab-main-window.ui:11
msgid "Clear Recent List"
msgstr "हालिया सूची रिक्त करें"

#: data/ui/baobab-main-window.ui:17 data/ui/baobab-preferences-dialog.ui:6
msgid "Preferences"
msgstr "सेटिंग्स"

#: data/ui/baobab-main-window.ui:21
msgid "Keyboard _Shortcuts"
msgstr "कुंजीपटल शॉर्टकट (_S)"

#: data/ui/baobab-main-window.ui:25
msgid "_Help"
msgstr "सहायता (_H)"

#: data/ui/baobab-main-window.ui:29
msgid "_About Disk Usage Analyzer"
msgstr "डिस्क उपयोग विश्लेषक के बारे में (_A)"

#: data/ui/baobab-main-window.ui:45 src/baobab-window.vala:540
msgid "Devices & Locations"
msgstr "उपकरण व स्थान"

#: data/ui/baobab-main-window.ui:55
msgid "Main Menu"
msgstr ""

#: data/ui/baobab-main-window.ui:97
#, fuzzy
#| msgid "Rescan current location"
msgid "Rescan Current Location"
msgstr "वर्तमान स्थान पुनः स्कैन करें"

#: data/ui/baobab-main-window.ui:107
msgid "Files may take more space than shown"
msgstr ""

#: data/ui/baobab-main-window.ui:275
msgid "Rings Chart"
msgstr "छल्लेनुमा चित्र"

#: data/ui/baobab-main-window.ui:288
msgid "Treemap Chart"
msgstr "शाखानुमा चित्र"

#: data/ui/baobab-preferences-dialog.ui:12
#| msgid "Locations to ignore"
msgid "Locations to Ignore"
msgstr "अनदेखी हेतु स्थान"

#: src/baobab-application.vala:33
msgid ""
"Do not skip directories on different file systems. Ignored if DIRECTORY is "
"not specified."
msgstr ""
"भिन्न फाइल सिस्टम होने पर डायरेक्टरी अनदेखी न करें। अनिर्दिष्ट होने पर ही डायरेक्टरी "
"अनदेखी करें।"

#: src/baobab-application.vala:34
msgid "Print version information and exit"
msgstr "संस्करण जानकारी दिखाएँ व बंद करें"

#: src/baobab-cellrenderers.vala:34
#, c-format
msgid "%d item"
msgid_plural "%d items"
msgstr[0] "%d वस्तु"
msgstr[1] "%d वस्तुएँ"

#. Translators: when the last modified time is unknown
#: src/baobab-cellrenderers.vala:40 src/baobab-location-list.vala:80
msgid "Unknown"
msgstr "अज्ञात"

#. Translators: when the last modified time is today
#: src/baobab-cellrenderers.vala:48
msgid "Today"
msgstr "आज"

#. Translators: when the last modified time is "days" days ago
#: src/baobab-cellrenderers.vala:53
#, c-format
msgid "%lu day"
msgid_plural "%lu days"
msgstr[0] "%lu दिन पूर्व"
msgstr[1] "%lu दिन पूर्व"

#. Translators: when the last modified time is "months" months ago
#: src/baobab-cellrenderers.vala:58
#, c-format
msgid "%lu month"
msgid_plural "%lu months"
msgstr[0] "%lu माह पूर्व"
msgstr[1] "%lu माह पूर्व"

#. Translators: when the last modified time is "years" years ago
#: src/baobab-cellrenderers.vala:62
#, c-format
msgid "%lu year"
msgid_plural "%lu years"
msgstr[0] "%lu वर्ष पूर्व"
msgstr[1] "%lu वर्ष पूर्व"

#: src/baobab-location-list.vala:67
#, c-format
msgid "%s Total"
msgstr "%s कुल"

#: src/baobab-location-list.vala:71
#, c-format
msgid "%s Available"
msgstr "%s उपलब्ध"

#. useful for some remote mounts where we don't know the
#. size but do have a usage figure
#: src/baobab-location-list.vala:85
#, c-format
msgid "%s Used"
msgstr "%s प्रयुक्त"

#: src/baobab-location-list.vala:87
msgid "Unmounted"
msgstr "माउंट नहीं है"

#: src/baobab-location.vala:72
#| msgid "Home folder"
msgid "Home Folder"
msgstr "होम फोल्डर"

#: src/baobab-location.vala:107
msgid "Computer"
msgstr "कंप्यूटर"

#. The only activatable row is "Add location"
#: src/baobab-preferences-dialog.vala:53
msgid "Select Location to Ignore"
msgstr "अनदेखी हेतु स्थान चयन चुनें"

#: src/baobab-preferences-dialog.vala:90
#| msgid "Add location…"
msgid "Add Location…"
msgstr "स्थान जोड़ें..."

#: src/baobab-window.vala:183
msgid "Select Folder"
msgstr "फोल्डर चुनें"

#: src/baobab-window.vala:190
msgid "Recursively analyze mount points"
msgstr "माउंट पॉइंट हेतु पुनरावर्तन विश्लेषण"

#: src/baobab-window.vala:214
#| msgid "Could not analyze volume."
msgid "Could not analyze volume"
msgstr "वॉल्यूम विश्लेषण विफल"

#: src/baobab-window.vala:276
msgid "translator-credits"
msgstr "Panwar108 <caspian7pena@gmail.com>, 2020"

#: src/baobab-window.vala:341
msgid "Failed to open file"
msgstr "फाइल खोलना विफल"

#: src/baobab-window.vala:358
#| msgid "Failed to open file"
msgid "Failed to trash file"
msgstr "फाइल थ्रैश करने में विफल"

#: src/baobab-window.vala:592
#| msgid "Could not scan folder “%s”"
msgid "Could not scan folder"
msgstr "फोल्डर स्कैन करने में विफल"

#: src/baobab-window.vala:608
msgid "Scan completed"
msgstr "स्कैन पूर्ण"

#: src/baobab-window.vala:609
#, c-format
msgid "Completed scan of “%s”"
msgstr "\"%s\" का स्कैन पूर्ण"

#: src/baobab-window.vala:649
#, c-format
msgid "“%s” is not a valid folder"
msgstr "\"%s\" अमान्य फोल्डर है"
